﻿using Lab5.Glowny.K;
using Lab5.Wyswietlacz.K;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Glowny.I
{
    public class Glowny : IGlowny
    {
        private IWyswietlacz wyswietlacz;

        public Glowny(IWyswietlacz wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }

        public string Method(string input)
        {
            return wyswietlacz.Wyswietl(input);
        }

        public string ZmienK(string hex)
        {
            return hex;
        }
    }
}
