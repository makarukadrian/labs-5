﻿using System;
using PK.Container;
using Kont;
using Lab5.Wyswietlacz.I;
using Lab5.Wyswietlacz.K;
using Lab5.Glowny.K;
using Lab5.Glowny;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer konte = new Container();

            IWyswietlacz wyswietlacz = new Wyświetlacz();
            IGlowny glowny = new Glowny(wyswietlacz);

            konte.Register(glowny);
            konte.Register(wyswietlacz);

            return konte;
        }
    }
}
