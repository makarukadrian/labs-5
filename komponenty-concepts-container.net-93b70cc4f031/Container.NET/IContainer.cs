﻿using System;
using System.Reflection;

namespace PK.Container
{
    public interface IContainer
    {
        /// <summary>
        /// Registering all interface implementations in assembly as providers of new instances.
        /// Resolve method will return new instance for each registered interface implementations.
        /// </summary>
        /// <param name="assembly">assembly with interface implementations</param>
        void Register(Assembly assembly);

        /// <summary>
        /// Registering class as provider of new instance of all implementing interfaces.
        /// Resolve method will return new instance of this class for each implemented interface.
        /// </summary>
        /// <param name="type">type of interface implementation to register</param>
        void Register(Type type);

        /// <summary>
        /// Registering object implementing interface T as provider of singleton.
        /// Resolve method will return exact this single instance for required interface T.
        /// </summary>
        /// <typeparam name="T">implementing interface</typeparam>
        /// <param name="impl">singleton instance</param>
        void Register<T>(T impl) where T : class;

        /// <summary>
        /// Registering object providing implementation of interface T.
        /// Resolve method will execute this instance of provider to obtain interface T implementation.
        /// </summary>
        /// <typeparam name="T">implementing interface</typeparam>
        /// <param name="provider">provider instance</param>
        void Register<T>(Func<T> provider) where T : class;


        /// <summary>
        /// Resolving instance of object implementing required interface
        /// throws UnresolvedDependenciesException in case of not resolved dependencies for required component
        /// </summary>
        /// <typeparam name="T">required interface</typeparam>
        /// <returns>object implementing required interface T or null if no implementation registered</returns>
        T Resolve<T>() where T : class;

        /// <summary>
        /// Resolving instance of object implementing required interface
        /// throws UnresolvedDependenciesException in case of not resolved dependencies for required component
        /// </summary>
        /// <param name="type">required interface</param>
        /// <returns>object implementing required interface T or null if no implementation registered</returns>
        object Resolve(Type type);
    }
}
