﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Glowny.K
{
    public interface IGlowny
    {
        string Method(string input);
        string ZmienK(string hex);
    }
}
