﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Kont
{
    public class Container : IContainer
    {
        private Dictionary<Type, Type> ClassInt = new Dictionary<Type, Type>();
        private Dictionary<Type, object> Interfejsy = new Dictionary<Type, object>();

        public void Register<T>(Func<T> provider) where T : class
        {
            T obiekt = provider.Invoke();
            Register<T>(obiekt);
        }
        public void Register<T>(T implementacja) where T : class
        {
            Type[] interfejs = implementacja.GetType().GetInterfaces();
            foreach (Type interfejs1 in interfejs)
            {
                if (!Interfejsy.ContainsKey(interfejs1))
                {
                    Interfejsy.Add(interfejs1, implementacja);
                }
                else
                {
                    Interfejsy[interfejs1] = implementacja;
                }
            }
        }
        public void Register(Type type)
        {
            Type[] localTypes = type.GetInterfaces();

            foreach (Type singleType in localTypes)
            {
                if (ClassInt.ContainsKey(singleType))
                {
                    ClassInt[singleType] = type;
                }

                else
                {
                    ClassInt.Add(singleType, type);
                }
            }
        }
        public void Register(Assembly assembly)
        {
            Type[] localTypes;
            localTypes = assembly.GetTypes();

            foreach (Type singleType in localTypes)
            {
                if (!singleType.IsNotPublic)
                {
                    this.Register(singleType);
                }
            }
        }
        public object Resolve(Type type)
        {
            if (Interfejsy.ContainsKey(type))
            {
                return Interfejsy[type];
            }

            else if (!ClassInt.ContainsKey(type))
            {
                return null;
            }

            else
            {
                ConstructorInfo[] konstr = ClassInt[type].GetConstructors();

                foreach (ConstructorInfo item in konstr)
                {
                    ParameterInfo[] info = item.GetParameters();

                    if (info.Length == 0)
                    {
                        return Activator.CreateInstance(ClassInt[type]);
                    }

                    List<object> lista = new List<object>(info.Length);

                    foreach (ParameterInfo tinfo in info)
                    {
                        var t = Resolve(tinfo.ParameterType);

                        if (t == null)
                        {
                            throw new UnresolvedDependenciesException();
                        }

                        lista.Add(t);
                    }

                    return item.Invoke(lista.ToArray());
                }
            }

            return null;
        }
        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }
    }
}
